# SLiM Themes

This repository brings together original themes from [SLiM legacy repository](https://sourceforge.net/projects/slim.berlios/files/) as well as some extra gentoo themes.
See [initial commit](a52cb606dfa6218235b8aab0127872e55a1bdb8c) for details.
All themes were slightly updated to fix some «*unknown option*» errors with [SLiM fork](https://slim-fork.sourceforge.io/).

### License
[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt). See also each theme folder where you may find additional (legacy) readmes/licenses/original-authors informations.

### What is SLiM?
[SLiM](https://slim-fork.sourceforge.io/) is an acronym for "Simple Login Manager". Lightweight and easily configurable, SLiM requires minimal dependencies, and none from the *GNOME* or *KDE* desktop environments. It therefore contributes towards a lightweight system for users that also like to use lightweight desktops such as *Xfce*, *Openbox*, and *Fluxbox*. 

### Previews

|capernoited|debian-moreblue|
|---|---|
|![img](previews/capernoited.jpg)|![img](previews/debian-moreblue.jpg)|

|fingerprint|flat|
|---|---|
|![img](previews/fingerprint.jpg)|![img](previews/flat.jpg)|

|flower2|gentoo|
|--|--|
|![img](previews/flower2.jpg)|![img](previews/gentoo.jpg)|

|gentoo_10_dark|isolated|
|--|--|
|![img](previews/gentoo_10_dark.jpg)|![img](previews/isolated.jpg)|

|lake|lotus-midnight|
|--|--|
|![img](previews/lake.jpg)|![img](previews/lotus-midnight.jpg)|

|lotus-sage|mindlock|
|--|--|
|![img](previews/lotus-sage.jpg)|![img](previews/mindlock.jpg)|

|parallel-dimensions|previous|
|--|--|
|![img](previews/parallel-dimensions.jpg)|![img](previews/previous.jpg)|

|rainbow|rear-window|
|--|--|
|![img](previews/rainbow.jpg)|![img](previews/rear-window.jpg)|

|scotland-road|slim-archlinux|
|--|--|
|![img](previews/scotland-road.jpg)|![img](previews/slim-archlinux.jpg)|

|slim-archlinux-simple|slim-gentoo-simple|
|--|--|
|![img](previews/slim-archlinux-simple.jpg)|![img](previews/slim-gentoo-simple.jpg)|

|slim-lunar|subway|
|--|--|
|![img](previews/slim-lunar.jpg)|![img](previews/subway.jpg)|

|wave|xfce-g-box|
|--|--|
|![img](previews/wave.jpg)|![img](previews/xfce-g-box.jpg)|

|Zenwalk|   |
|--|--|
|![img](previews/Zenwalk.jpg)|![img](previews/trans.png)|

<br />
